//
//  Spot.swift
//  SurfApp
//
//  Created by Ryo on 17/02/2021.
//

import Foundation

struct Spot {
    var name: String
    var country: String
}
