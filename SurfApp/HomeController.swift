//
//  ViewController.swift
//  SurfApp
//
//  Created by Ryo on 17/02/2021.
//

import UIKit

class HomeController: UIViewController {

    @IBOutlet weak var contactButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactButton.layer.cornerRadius = 20
    }


}

