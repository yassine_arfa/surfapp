//
//  SurferDetailController.swift
//  SurfApp
//
//  Created by Ryo on 18/02/2021.
//

import UIKit

class SurferDetailController: UIViewController {

    
    @IBOutlet weak var bgIV: UIImageView!
    @IBOutlet weak var profileIV: RoundedImage!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var natLbl: UILabel!
    
    var surfer: Surfer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bgIV.from(surfer.bg)
        profileIV.from(surfer.profile)
        natLbl.text = surfer.nationality
        nameLbl.text = surfer.surname + " " + surfer.name
        
    }


}
