//
//  Surfer.swift
//  SurfApp
//
//  Created by Ryo on 17/02/2021.
//

import Foundation

struct Surfer {
    var name: String
    var surname: String
    
    var profile: String {
        return name + "-profile"
    }
    
    var bg: String {
        return name + "-bg"
    }
    
    var nationality: String
}


