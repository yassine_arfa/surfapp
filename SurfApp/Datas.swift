//
//  Datas.swift
//  SurfApp
//
//  Created by Ryo on 17/02/2021.
//

import Foundation

func surfers() ->[Surfer] {
    return [
        Surfer(name: "Erickson", surname: "Sage", nationality: "USA"),
        Surfer(name: "Fitzgibbons", surname: "Sally", nationality: "Australia"),
        Surfer(name: "Gabeira", surname: "Maya", nationality: "Brazil"),
        Surfer(name: "Gilmore", surname: "Stephanie", nationality: "Australia"),
        Surfer(name: "Manuel", surname: "Malia", nationality: "USA"),
        Surfer(name: "Quizon", surname: "Alessa", nationality: "USA")
    
    
    ]
    
    
}


var spots: [Spot] = [
    Spot(name: "Bells Beach", country: "Australia"),
    Spot(name: "Hossegor", country: "France"),
    Spot(name: "Mentawai", country: "Indonésie"),
    Spot(name: "Pipeline", country: "Hawaii"),
    Spot(name: "Teahupoo", country: "Polynésie"),
    Spot(name: "Tenerife", country: "Espagne"),
]
