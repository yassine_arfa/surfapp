//
//  SpotsController.swift
//  SurfApp
//
//  Created by Ryo on 17/02/2021.
//

import UIKit

class SpotsController: UIViewController {

    @IBOutlet weak var spotName: UILabel!
    @IBOutlet weak var spotIV: UIImageView!
    
    
    @IBOutlet var buttons: [UIButton]!
    
    var spotIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButtons()
        setupSpot()
    }
    
    func setupSpot() {
        spotName.text = spots[spotIndex].name
        spotIV.from(spots[spotIndex].name.lowercased())
    }
    
    
    func setupButtons() {
        buttons.forEach { (button) in
            let spot = spots[button.tag]
            let image = UIImage(named: spot.name.lowercased())
            button.setImage(image, for: .normal)
        }
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        spotIndex = sender.tag
        setupSpot()
    }
}
