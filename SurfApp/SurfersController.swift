//
//  SurfersController.swift
//  SurfApp
//
//  Created by Ryo on 17/02/2021.
//

import UIKit

class SurfersController: UIViewController {

    @IBOutlet weak var rankingLabel: UILabel!
    @IBOutlet weak var surferIV: RoundedImage!
    @IBOutlet weak var surferName: UILabel!
    
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSurfer()
        surferIV.isUserInteractionEnabled = true
        surferIV.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(surferTapped)))        // Do any additional setup after loading the view.
    }
    
    func setupSurfer() {
        let surfer = surfers()[currentIndex]
        rankingLabel.text = "Classement: \(currentIndex + 1)"
        surferIV.from(surfer.profile)
        surferName.text = surfer.surname + " " + surfer.name
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "detail" else { return }
        guard let next = segue.destination as? SurferDetailController else { return }
        next.surfer = surfers()[currentIndex]
    }
    
    @objc func surferTapped() {
        performSegue(withIdentifier: "detail", sender: nil)
    }
    
    @IBAction func previousSurfer(_ sender: Any) {
        if currentIndex != 0 {
            currentIndex -= 1
            setupSurfer()
        }
    }
    
    @IBAction func nextSurfer(_ sender: Any) {
        if currentIndex < surfers().count - 1 {
            currentIndex += 1
            setupSurfer()
        }
    }
    
    
}
